package com.cejv416.loancalculatorfx.app;

import com.cejv416.loancalculatorfx.calculator.Calculations;
import com.cejv416.loancalculatorfx.data.FinanceBean;
import com.cejv416.loancalculatorfx.presentation.LoanCalculatorFX;
import javafx.application.Application;
import javafx.stage.Stage;

/**
 * JavaFX applications do not use main() the same way as basic Java applications
 * do. The main() has a single call to a start() method. In this example start()
 * will call upon a method in a separate class to build the UI and run the
 * program.
 *
 * @author Ken Fogel
 */
public class LoanCalculatorMain extends Application {

    private Calculations calc;
    private FinanceBean finance;
    private LoanCalculatorFX gui;

    /**
     * Rather than a constructor, a class that extends Application uses an init
     * method.
     */
    @Override
    public void init() {
        calc = new Calculations();
        finance = new FinanceBean();
        gui = new LoanCalculatorFX(calc, finance);
    }

    /**
     * The start method must be overriden in a class that extends Application
     *
     * @param primaryStage
     */
    @Override
    public void start(Stage primaryStage) {
        gui.start(primaryStage);
    }

    /**
     * Where is all begins but this time there is only one line of code.
     *
     * @param args
     */
    public static void main(String[] args) {
        Application.launch(args);
    }
}
