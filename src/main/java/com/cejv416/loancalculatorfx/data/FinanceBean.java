package com.cejv416.loancalculatorfx.data;

import java.math.BigDecimal;

public class FinanceBean {

    private BigDecimal inputValue; //references
    private BigDecimal rate;
    private BigDecimal term;
    private BigDecimal result;

    public FinanceBean() {
        result = new BigDecimal("100");
        inputValue = new BigDecimal("0");
        rate = new BigDecimal("0");
        term = new BigDecimal("0");
    }

    public BigDecimal getResult() {
        return result;
    }

    public void setResult(BigDecimal result) {
        this.result = result;
    }

    public BigDecimal getInputValue() {
        return inputValue;
    }

    public void setInputValue(BigDecimal inputValue) {
        this.inputValue = inputValue;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public BigDecimal getTerm() {
        return term;
    }

    public void setTerm(BigDecimal term) {
        this.term = term;
    }
}
