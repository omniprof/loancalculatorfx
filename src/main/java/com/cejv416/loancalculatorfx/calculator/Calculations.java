package com.cejv416.loancalculatorfx.calculator;

import com.cejv416.loancalculatorfx.data.FinanceBean;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

public class Calculations {

    /**
     * The Loan calculation
     *
     * @param money
     * @throws ArithmeticException
     */
    public void loanCalculation(FinanceBean money) throws ArithmeticException {

        // Divide APR by 12
        BigDecimal monthlyRate = money.getRate().divide(new BigDecimal("12"), MathContext.DECIMAL64);

        // At each step this variable is updated
        BigDecimal temp;
        // (1+rate)
        temp = BigDecimal.ONE.add(monthlyRate);

        // (1+rate)^term
        temp = temp.pow(money.getTerm().intValueExact());

        // BigDecimal pow does not support negative exponents so divide 1 by the result
        temp = BigDecimal.ONE.divide(temp, MathContext.DECIMAL64);

        // 1 - (1+rate)^-term
        temp = BigDecimal.ONE.subtract(temp);

        // rate / (1 - (1+rate)^-term)
        temp = monthlyRate.divide(temp, MathContext.DECIMAL64);

        // pv * (rate / 1 - (1+rate)^-term)
        temp = money.getInputValue().multiply(temp);

        // Round to 2 decimal places using banker's rounding
        temp = temp.setScale(2, RoundingMode.HALF_EVEN);

        // Remove the sign if the result is negative
        money.setResult(temp.abs());
    }

}
