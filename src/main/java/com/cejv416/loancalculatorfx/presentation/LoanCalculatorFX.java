package com.cejv416.loancalculatorfx.presentation;

import com.cejv416.loancalculatorfx.calculator.Calculations;
import com.cejv416.loancalculatorfx.data.FinanceBean;
import java.math.BigDecimal;
import javafx.application.Platform;

import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

/**
 *
 * @author Ken
 */
public class LoanCalculatorFX {

    private final Calculations calc;
    private final FinanceBean finance;

    private TextField loanValue;
    private TextField rateValue;
    private TextField termValue;
    private TextField resultValue;

    /**
     * Constructor that receives the action and data objects
     *
     * @param calc
     * @param finance
     */
    public LoanCalculatorFX(Calculations calc, FinanceBean finance) {
        this.calc = calc;
        this.finance = finance;
    }

    /**
     * This method creates a Label that is centered inside an HBox.
     *
     * @param text
     * @return
     */
    private HBox createTitle(String text) {
        Label title = new Label(text);

        // Style the label using CSS
        // Possible fonts can be found at http://www.webdesigndev.com/16-gorgeous-web-safe-fonts-to-use-with-css/
        title.setStyle("-fx-font-size:18pt; -fx-font-weight:bold; -fx-font-family:Century Gothic, sans-serif");

        // To center the title and give it padding create an HBox, set the
        // padding and alignment, add the label and then add the HBox to the BorderPane.
        HBox hbox = new HBox();
        hbox.getChildren().add(title);
        hbox.setAlignment(Pos.CENTER);
        hbox.setPadding(new Insets(20.0));

        return hbox;
    }

    /**
     * This method creates a BorderPane that has a title in the top and a
     * GridPane in the center. The GridPane contains a form.
     *
     * @return The BorderPane to add to the Scene
     */
    private BorderPane buildLoan() {

        // Create the pane that will hold the controls
        BorderPane loanPane = new BorderPane();

        // Add a Title
        loanPane.setTop(createTitle("Loan Calculations"));

        // Craete an empty GridPane
        GridPane loanGrid = new GridPane();

        // Column 0, Row 0
        Label loanLabel = new Label("Loan Amount");
        loanLabel.setStyle("-fx-font-size:14pt; -fx-font-weight:bold; -fx-font-family:Century Gothic, sans-serif");
        loanGrid.add(loanLabel, 0, 0);

        // Column 1, Row 0
        loanValue = new TextField();
        loanValue.setStyle("-fx-font-size:14pt; -fx-font-weight:normal; -fx-font-family:Century Gothic, sans-serif");
        loanValue.setAlignment(Pos.CENTER_RIGHT);
        loanGrid.add(loanValue, 1, 0);

        // Column 0, Row 1
        Label rateLabel = new Label("Interest Rate");
        rateLabel.setStyle("-fx-font-size:14pt; -fx-font-weight:bold; -fx-font-family:Century Gothic, sans-serif");
        loanGrid.add(rateLabel, 0, 1);

        // Column 1, Row 1
        rateValue = new TextField();
        rateValue.setStyle("-fx-font-size:14pt; -fx-font-weight:normal; -fx-font-family:Century Gothic, sans-serif");
        rateValue.setAlignment(Pos.CENTER_RIGHT);
        loanGrid.add(rateValue, 1, 1);

        // Column 0, Row 2
        Label termLabel = new Label("Term In Months");
        termLabel.setStyle("-fx-font-size:14pt; -fx-font-weight:bold; -fx-font-family:Century Gothic, sans-serif");
        loanGrid.add(termLabel, 0, 2);

        // Column 1, Row 2
        termValue = new TextField();
        termValue.setStyle("-fx-font-size:14pt; -fx-font-weight:normal; -fx-font-family:Century Gothic, sans-serif");
        termValue.setAlignment(Pos.CENTER_RIGHT);
        loanGrid.add(termValue, 1, 2);

        // Column 0, Row 3
        Label resultLabel = new Label("Payment");
        resultLabel.setStyle("-fx-font-size:14pt; -fx-font-weight:bold; -fx-font-family:Century Gothic, sans-serif");
        loanGrid.add(resultLabel, 0, 3);

        // Column 1, Row 3
        resultValue = new TextField();
        resultValue.setStyle("-fx-font-size:14pt; -fx-font-weight:normal; -fx-font-family:Century Gothic, sans-serif");
        resultValue.setAlignment(Pos.CENTER_RIGHT);
        resultValue.setEditable(false);
        loanGrid.add(resultValue, 1, 3);

        // Create a button and attach an event handler
        Button calculate = new Button("Calculate");
        calculate.setStyle("-fx-font-size:14pt; -fx-font-weight:bold; -fx-font-family:Century Gothic, sans-serif");
        calculate.setOnAction(this::calculateButtonHandler);

        // Create a button and attach an event handler
        Button exit = new Button("Exit");
        exit.setStyle("-fx-font-size:14pt; -fx-font-weight:bold; -fx-font-family:Century Gothic, sans-serif");
        exit.setOnAction(this::exitButtonHandler);

        // HBox that will span 2 columns so thatbutton can be centered across the GridPane
        HBox hbox = new HBox();
        hbox.getChildren().addAll(calculate, exit);
        hbox.setSpacing(20.0); // Spacing between controls in the HBox
        hbox.setAlignment(Pos.CENTER);
        hbox.setPadding(new Insets(20.0)); // Spacing around the controls in the HBox
        loanGrid.add(hbox, 0, 4, 2, 1);

        // Set the column widths as a percentage
        ColumnConstraints col1 = new ColumnConstraints();
        col1.setPercentWidth(30.0);
        ColumnConstraints col2 = new ColumnConstraints();
        col2.setPercentWidth(70.0);

        loanGrid.getColumnConstraints().addAll(col1, col2);

        // Add space around the outside of the GridPane
        loanGrid.setPadding(new Insets(20.0));
        // Add space between rows and columns of the GridPane
        loanGrid.setHgap(10.0);
        loanGrid.setVgap(10.0);

        // Load the GridPane into the pane
        loanPane.setCenter(loanGrid);

        return loanPane;
    }

    /**
     * This is the event handler when the button is being pressed. If there is a
     * NumberFormatException when any of the fields is converted to a BigDecimal
     * then an Alert box is displayed
     *
     * There is a better way to handle strings that cannot convert and you will
     * learn how in the Java Desktop course.
     *
     * @param e
     */
    private void calculateButtonHandler(ActionEvent e) {
        boolean doCalculation = true;
        try {
            finance.setInputValue(new BigDecimal(loanValue.getText()));
        } catch (NumberFormatException nfe) {
            doCalculation = false;
            numberFormatAlert(loanValue.getText(), "Loan");
        }
        try {
            finance.setRate(new BigDecimal(rateValue.getText()));
        } catch (NumberFormatException nfe) {
            doCalculation = false;
            numberFormatAlert(rateValue.getText(), "Rate");
        }
        try {
            finance.setTerm(new BigDecimal(termValue.getText()));
        } catch (NumberFormatException nfe) {
            doCalculation = false;
            numberFormatAlert(termValue.getText(), "Term");
        }

        if (doCalculation == true) {
            calc.loanCalculation(finance);
            resultValue.setText(finance.getResult().toString());
        }
    }

    /**
     * This is the event handler for the Exit button. It ends the program
     *
     * @param e
     */
    private void exitButtonHandler(ActionEvent e) {
        Platform.exit();
    }

    /**
     * Display an Alert box if there is a NumberFormatException detected in the
     * calculateButtonHandler
     *
     * @param badValue
     * @param textField
     */
    private void numberFormatAlert(String badValue, String textField) {
        Alert alert = new Alert(AlertType.ERROR);
        alert.setTitle("Number Format Error");
        alert.setHeaderText("The value \"" + badValue + "\" cannot be converted to a number for the " + textField);
        alert.setContentText("Number Format Error");

        alert.showAndWait();
    }

    /**
     * The primary method that constructs the user interface
     *
     * @param primaryStage
     */
    public void start(Stage primaryStage) {

        Parent root = buildLoan();
        Scene scene = new Scene(root, 600, 450);

        primaryStage.setTitle("Calculations");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
